﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Limo_App.Startup))]
namespace Limo_App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
           
        }
    }
}
