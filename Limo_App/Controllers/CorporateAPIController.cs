﻿using Limo_BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Limo_App.Models;

namespace Limo_App.Controllers
{
    public class CorporateAPIController : ApiController
    {
        public readonly ICorporateClientBAL objICorporateClient = null;
        public CorporateAPIController(ICorporateClientBAL _ICorporateClientBAL)
        {
            objICorporateClient = _ICorporateClientBAL;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        public Response GetJobDetails(int jobId)
        {
            try
            {
                var driversJobs = objICorporateClient.getBookingListByID(jobId, string.Empty);

                return Response.Succeed().AddData("driverJobs", driversJobs);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
