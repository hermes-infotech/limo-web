﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Limo_App.Controllers
{
    
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Dashboard()
        {
            return View();
        }
       
        

        public ActionResult IndividualUserEditProfile()
        {
            return View();
        }

        public ActionResult ClientEditProfile()
        {
            return View();
        }
    }
}