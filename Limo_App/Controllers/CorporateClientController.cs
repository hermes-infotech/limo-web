﻿using Limo_App.Models;
using Limo_BusinessLayer.Interfaces;
using Limo_DTO.CorporateClient;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Limo_App.Controllers
{
    public class CorporateClientController : Controller
    {
        private readonly ICorporateClientBAL objICorporateClient = null;
        public CorporateClientController(ICorporateClientBAL _ICorporateClientBAL)
        {
            objICorporateClient = _ICorporateClientBAL;
        }
        public ActionResult AddCorporateClient()
        {
            return View();
        }
        public ActionResult CorporateClientList()
        {
            return View();
        }
        public ActionResult BookingList()
        {
            return View();
        }
        public ActionResult AddBooking()
        {
            return View();
        }
        public ActionResult AddBankUser()
        {
            return View();
        }
        public ActionResult JobList()
        {
            return View();
        }
        [HttpGet]
        public JsonResult getCorporateClientList()
        {
            try
            {
                var UserID = User.Identity.GetUserId();
                return Json(objICorporateClient.getCorporateClientList(UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult getCorporateClientListByID(int ID)
        {
            try
            {
                var EmailID = User.Identity.Name;
                var UserID = User.Identity.GetUserId();
                return Json(objICorporateClient.getCorporateClientListByID(ID, UserID, EmailID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Delete the records by id    
        [HttpGet]
        public JsonResult DeleteCorporateClientDetail(int ID)
        {
            try
            {
                var UserID = User.Identity.GetUserId();
                return Json(objICorporateClient.DeleteCorporateClientDetail(ID, UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult SaveCorporateClientDetail(CorporateClientInfo CorporateClientDTO, HttpPostedFileBase LogoImageFile)
        {
            try
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<AspNetUser>(new UserStore<AspNetUser>(context));
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                
                if (LogoImageFile != null)
                {
                    string[] ext = LogoImageFile.ContentType.Split('/');
                    string extention = '.' + ext[1];
                    string[] extEmail = CorporateClientDTO.Email.Split('@');
                    string FileName = "CorporateClient" + extEmail[0] + DateTime.Now.ToString("h_mm_sstt") + extention;
                    FileName = string.Format(FileName, extention).ToString();
                    LogoImageFile.SaveAs(Server.MapPath("~/Content/img/UserImage/") + FileName);
                    CorporateClientDTO.ProfilePathImg = FileName;
                }
                var CorporateClient = objICorporateClient.SaveCorporateClientDetail(CorporateClientDTO, User.Identity.GetUserId());

                if (CorporateClientDTO.ID != null)
                {
                    var UserID = User.Identity.GetUserId();
                    var user = UserManager.FindByEmail(CorporateClientDTO.Email);
                    user.Profile = CorporateClientDTO.ProfilePathImg;

                    if (user.Id == UserID)
                    {
                        Session["profile"] = user.Profile != null ? user.Profile : "";
                    }
                    UserManager.Update(user);
                }


                if (CorporateClient == 1)
                {
                    var user = new AspNetUser { UserName = CorporateClientDTO.Email, Email = CorporateClientDTO.Email , Profile = CorporateClientDTO.ProfilePathImg };
                    var chkUser = UserManager.Create(user, CorporateClientDTO.Password);
                    if (chkUser.Succeeded)
                    {
                        var result1 = UserManager.AddToRole(user.Id, "CorporateClient");
                        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "CorporateClient";
                        roleManager.Create(role);
                    }
                }
                if (CorporateClientDTO.Flag == "0")
                {
                    return RedirectToAction("Dashboard", "Home", new { Area = "", status = "" });
                }
                else
                {
                    return RedirectToAction("CorporateClientList", "CorporateClient", new { Area = "", status = "" });
                }
                
            }
            catch (Exception ex)
            {
                return RedirectToAction("AddCorporateClient", "CorporateClient", new { Area = "", status = "N" });
            }
        }

        [HttpPost]
        public ActionResult SaveBookingDetail(BookingInfo BookingInfoDTO)
        {
            try
            {
               
                var Booking = objICorporateClient.SaveBookingDetail(BookingInfoDTO, User.Identity.GetUserId());
                //if (Booking == 1)
                //{
                //    var user = new ApplicationUser { UserName = CorporateClientDTO.Email, Email = CorporateClientDTO.Email };
                //    var chkUser = UserManager.Create(user, CorporateClientDTO.Password);
                //    if (chkUser.Succeeded)
                //    {
                //        var result1 = UserManager.AddToRole(user.Id, "CorporateClient");
                //        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                //        role.Name = "CorporateClient";
                //        roleManager.Create(role);
                //    }
                //}
                if (BookingInfoDTO.Flag == "0")
                {
                    return RedirectToAction("Dashboard", "Home", new { Area = "", status = "" });
                }
                else
                {
                    return RedirectToAction("BookingList", "CorporateClient", new { Area = "", status = "" });
                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("AddBooking", "CorporateClient", new { Area = "", status = "N" });
            }
        }
        public JsonResult getBookingList()
        {
            try
            {
                var UserID="";
                if (User.IsInRole("Admin") == true)
                {
                    UserID = null;
                }
                else
                {
                    UserID = User.Identity.GetUserId();
                }
                return Json(objICorporateClient.getBookingList(UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult getBookingListByID(int ID)
        {
            try
            {
                var UserID = User.Identity.GetUserId();
                return Json(objICorporateClient.getBookingListByID(ID, UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Delete the records by id    
        [HttpGet]
        public JsonResult DeleteBookingDetail(int ID)
        {
            try
            {
                var UserID = User.Identity.GetUserId();
                return Json(objICorporateClient.DeleteBookingDetail(ID, UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult getJobList()
        {
            try
            {
                var UserID = "";
                if (User.IsInRole("Admin") == true)
                {
                    UserID = null;
                }
                else
                {
                    UserID = User.Identity.GetUserId();
                }
                return Json(objICorporateClient.getJobList(UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                int status;
                return status = objICorporateClient.IsEmailExist(ID, EmailID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}