﻿using Limo_App.Models;
using Limo_BusinessLayer.Interfaces;
using Limo_DTO.Driver;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Limo_App.Controllers
{
    [Authorize]
    public class DriverController : Controller
    {
        private readonly IDriverBAL objIDriver = null;
        public DriverController(IDriverBAL _IDriverBAL)
        {
            objIDriver = _IDriverBAL;
        }
        public ActionResult AddDriver()
        {
            return View();
        }
        [Authorize]
        public ActionResult DriverList()
        {
            return View();
        }
        [HttpGet]
        public JsonResult getDriverList()
        {
            try
            {
                var UserID = "";
                if (User.IsInRole("Admin") == true)
                {
                    UserID = null;
                }
                else
                {
                    UserID = User.Identity.GetUserId();
                }
                return Json(objIDriver.getDriverList(UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult getDriverListByID(int ID)
        {
            try
            {
                var EmailID = User.Identity.GetUserName();
                var UserID = User.Identity.GetUserId();
                return Json(objIDriver.getDriverListByID(ID, UserID, EmailID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Delete the records by id    
        [HttpGet]
        public JsonResult DeleteDriverDetail(int ID)
        {
            try
            {
                var UserID = User.Identity.GetUserId();
                return Json(objIDriver.DeleteDriverDetail(ID, UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult SaveDriverDetail(DriverInfo DriverDTO, HttpPostedFileBase LogoImageFile)
        {
            try
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<AspNetUser>(new UserStore<AspNetUser>(context));
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                var UserID = User.Identity.GetUserId();
                if (LogoImageFile != null)
                {
                    string[] ext = LogoImageFile.ContentType.Split('/');
                    string extention = '.' + ext[1];
                    string[] extEmail = DriverDTO.Email.Split('@');
                    string FileName = "Driver" + extEmail[0] + DateTime.Now.ToString("h_mm_sstt") + extention;
                    FileName = string.Format(FileName, extention).ToString();
                    LogoImageFile.SaveAs(Server.MapPath("~/Content/img/UserImage/") + FileName);
                    DriverDTO.ProfilePathImg = FileName;
                }
                var Driver = objIDriver.SaveDriverDetail(DriverDTO,UserID);
                if (DriverDTO.ID != null)
                {
                    
                    var user = UserManager.FindByEmail(DriverDTO.Email);
                    user.Profile = DriverDTO.ProfilePathImg;

                    if (user.Id == UserID)
                    {
                        Session["profile"] = user.Profile != null ? user.Profile : "";
                    }
                    UserManager.Update(user);
                }
                if (Driver == 1)
                {
                    var user = new AspNetUser { UserName = DriverDTO.Email, Email = DriverDTO.Email, Profile = DriverDTO.ProfilePathImg };
                    var chkUser = UserManager.Create(user, DriverDTO.Password);
                    if (chkUser.Succeeded)
                    {
                        var result1 = UserManager.AddToRole(user.Id, "Driver");
                        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "Driver";
                        roleManager.Create(role);

                    }   
                }
                if (DriverDTO.Flag == "0")
                {
                    return RedirectToAction("Dashboard", "Home", new { Area = "", status = "" });
                }
                else
                {
                    return RedirectToAction("DriverList", "Driver", new { Area = "", status = "" });
                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("AddDriver", "Driver", new { Area = "", status = "N" });
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                int status;
                return status = objIDriver.IsEmailExist(ID, EmailID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}