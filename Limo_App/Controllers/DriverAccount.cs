﻿using System;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Limo_DataLayer.Entity;
using System.Web.Http;
using Limo_BusinessLayer.Interfaces;
using Limo_DTO.CorporateClient;
using System.Collections.Generic;
using System.Net.Http;
using Limo_App.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO;
using System.Net.Mail;
using System.Web.Mvc;
using System.Threading.Tasks;
using Limo_DTO.Driver;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity;

namespace Limo_App.Controllers
{
    public class DriverAccountController : ApiController
    {
        private Entities LimoEntitiesdb = new Entities();
        private readonly IDriverBAL objIDriver = null;
        //public readonly ICorporateClientBAL objICorporateClient=null;
        //public DriverAccountController(ICorporateClientBAL _ICorporateClientBAL)
        //{
        //    objICorporateClient = _ICorporateClientBAL;
        //}

        public DriverAccountController(IDriverBAL _IDriverBAL)
        {
            objIDriver = _IDriverBAL;
        }


        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
       

        public DriverAccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? System.Web.HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        // POST api/<controller>/Login
        [System.Web.Http.AllowAnonymous]
        [System.Web.Http.HttpPost]
        public async Task<Response> Login(LoginViewModel Model)
        {
            try
            {
                if (Model.Email != null && Model.Password != null)
                {
                    if (!ModelState.IsValid)
                    {
                        return Response.Fail();
                    }
                    // var rememeber = false;
                    var result = await SignInManager.PasswordSignInAsync(Model.Email, Model.Password, Model.RememberMe, shouldLockout: false);

                    // var result = SignInManager.PasswordSignInAsync(Email, Password, rememeber, shouldLockout: false);
                    var status = LimoEntitiesdb.UserStatus.Where(x => x.Email == Model.Email && x.roleID == 4).ToList();
                    if (status != null)
                    {
                        if (status[0].Status == true)
                        {
                            switch (result)
                            {
                                case SignInStatus.Success:

                                    return Response.Succeed().AddData("LoginDetails", status);


                                case SignInStatus.LockedOut:

                                    return Response.Succeed().AddData("LoginDetails", status);

                                case SignInStatus.Failure:
                                default:

                                    return Response.Fail(CommonEnums.ApiStatusCode.LOGIN_FAILED, "Invalid Login Attempt");
                            }
                        }
                        else
                        {
                            // ModelState.AddModelError("", "Invalid login attempt.");
                            return Response.Fail(CommonEnums.ApiStatusCode.LOGIN_FAILED, "Invalid Login Attempt");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Enter valida email and password");
                        return Response.Fail(CommonEnums.ApiStatusCode.LOGIN_FAILED, "Invalid Login Attempt");
                    }

                }
                else
                {
                    return Response.Fail(CommonEnums.ApiStatusCode.LOGIN_FAILED, "Enter email and Password");

                }
            }
            catch (Exception ex)
            {
                return Response.Fail();
            }

        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        // GET api/<controller>/JobList
        public Response JobList(string Email)
        {
            //var UserID = User.Identity.GetUserId();
            // var UserID = "";
            // UserID = null;
            BookingInfo objBookingInfo = new BookingInfo();
            var booking = LimoEntitiesdb.GetBookingListForApi(Email).ToList();
            // var bookings = objICorporateClient.getBookingListForApi(Email);
            var bDetail = LimoEntitiesdb.BookingDetails.ToList();
            var dj = LimoEntitiesdb.DriversJobs.ToList();
            var dDetail = LimoEntitiesdb.DriverDetails.ToList();
            var result = (from j in bDetail
                          join bd in dj on j.BookingID equals bd.Driver_Id
                          join dd in dDetail on bd.Driver_Id equals dd.ID
                          select new
                          {
                              j.EmailID,
                              j.PassengerName,
                              j.ContactNumber,
                              j.DestinationLocation,
                              j.Airport,
                              j.Car,
                              j.Date
                              //objBookingInfo.BookingID = BookingListDetails.BookingID;

                          });

            var data = result.Where(x => x.EmailID == Email);



            return Response.Succeed().AddData("BookingLists", data);



        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        // GET api/<controller>/profiledetail
        public Response profiledetail(string email)
        {
            var profiledetails = LimoEntitiesdb.DriverDetails.Where(x => x.Email.ToLower() == email.ToLower()).ToList();

            return Response.Succeed().AddData("profiledetails", profiledetails);

        }

        [System.Web.Http.HttpPut]
        // post api/<controller>/updateProfile
        public Response updateProfile(DriverInfo DriverDTO, HttpPostedFileBase LogoImageFile)
        {
            try
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<Models.AspNetUser>(new UserStore<Models.AspNetUser>(context));
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                var UserID = User.Identity.GetUserId();
                if (LogoImageFile != null)
                {
                    string[] ext = LogoImageFile.ContentType.Split('/');
                    string extention = '.' + ext[1];
                    string[] extEmail = DriverDTO.Email.Split('@');
                    string FileName = "Driver" + extEmail[0] + DateTime.Now.ToString("h_mm_sstt") + extention;
                    FileName = string.Format(FileName, extention).ToString();
                    LogoImageFile.SaveAs(HttpContext.Current.Server.MapPath("~/Content/img/UserImage/") + FileName);
                    DriverDTO.ProfilePathImg = FileName;
                }
                var Driver = objIDriver.SaveDriverDetail(DriverDTO, UserID);
                if (DriverDTO.ID != null)
                {
                    var Session = HttpContext.Current.Session;
                    var user = UserManager.FindByEmail(DriverDTO.Email);
                    user.Profile = DriverDTO.ProfilePathImg;

                    if (user.Id == UserID)
                    {


                        Session["profile"] = user.Profile != null ? user.Profile : "";
                    }
                    UserManager.Update(user);
                }
                if (Driver == 1)
                {
                    var user = new Models.AspNetUser { UserName = DriverDTO.Email, Email = DriverDTO.Email, Profile = DriverDTO.ProfilePathImg };
                    var chkUser = UserManager.Create(user, DriverDTO.Password);
                    if (chkUser.Succeeded)
                    {
                        var result1 = UserManager.AddToRole(user.Id, "Driver");
                        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "Driver";
                        roleManager.Create(role);

                    }
                }
                if (DriverDTO.Flag == "0")
                {
                    return Response.Fail(CommonEnums.ApiStatusCode.SERVER_ERROR, "Something went wrong");
                }
                else
                {
                    var user = UserManager.FindByEmail(DriverDTO.Email);
                    return Response.Succeed().AddData("driverProfile", user);
                }

            }
            catch (Exception ex)
            {
                Response.Fail(CommonEnums.ApiStatusCode.SERVER_ERROR, "Something went wrong");
            }
            return Response.Succeed();
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        // POST api/<controller>/ForgotPassword
        public Response ForgotPassword(ForgotPasswordViewModel Model)
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            var newPassword = path.Substring(0, 8);

            // Get the existing student from the db
            var user = UserManager.FindByEmail(Model.Email);

            // Update it with the values from the view model
            String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            user.PasswordHash = hashedNewPassword;
            MailMessage mail = new MailMessage();
            mail.To.Add(Model.Email);
            mail.From = new MailAddress("awnish@arshtech.com");
            mail.Subject = "New Password";
            string Body = "Hi" + Model.Email + "  your new Password is <b> " + newPassword + " <b>";
            mail.Body = Body;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.elasticemail.com";
            smtp.Port = 2525;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential("awnish@arshtech.com", "e3a5342b-7d95-4de6-a854-452bb131ad4b"); // Enter seders User name and password   
            smtp.EnableSsl = true;
            smtp.Send(mail);

            // Apply the changes if any to the db
            UserManager.Update(user);


            //var pass = "1@Adfgg";            
            //var user = UserManager.FindByEmail(Email);
            //var Token = UserManager.GeneratePasswordResetToken(user.Id);
            //var result =  UserManager.ResetPasswordAsync(user.Id,Token, pass);
            //  UserManager.Update();

            // String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            // UserStore<UserManager<Models> store = new UserStore<UserManager>();
            // store.SetPasswordHashAsync(cUser, hashedNewPassword);
            return Response.Succeed().AddData("ResetPassword", mail);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.AllowAnonymous]
        public Response DriverAction(DriverJobActionModel djModel)
        {
            try
            {
                var UserID = djModel.UserId;
                var s = objIDriver.DriverAction(djModel, UserID);
                var x = new
                {
                    status = s
                };
                return Response.Succeed().AddData("DriverAction", x);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        public Response GetDriverJobList(int userid)
        {
            try
            {
                var driversJobs = objIDriver.GetDriverJobList(userid);
               
                return Response.Succeed().AddData("driverJobs", driversJobs);
            }
            catch (Exception)
            {

                throw;
            }
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.AllowAnonymous]
        public Response SaveDriverRating(DriverRatingModel drModel)
        {
            try
            {
                var s = objIDriver.SaveDriverRating(drModel);

                var status = s == 1 ? "Rating saved successfully" : "Failed to save driver rating.";
                
                return Response.Succeed().AddData("Message", status);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.AllowAnonymous]
        public Response GetDriverDetail(int driverId)
        {
            try
            {
                var driversDetail = objIDriver.getDriverListByID(driverId,"","");

                return Response.Succeed().AddData("DriversDetail", driversDetail);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}