﻿using Limo_BusinessLayer.Interfaces;
using Limo_DTO.IndividualClient;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Web.Mvc;
using Limo_App.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Limo_App.Controllers
{
    public class IndividualClientController : Controller
    {
        private readonly IIndividualClientBAL objIIndividualClient = null;
        public IndividualClientController(IIndividualClientBAL _IIndividualClientBAL)
        {
            objIIndividualClient = _IIndividualClientBAL;
        }
        public ActionResult IndividualClientList()
        {
            return View();
        }
        public ActionResult AddIndividualClientList()
        {
            return View();
        }
        [HttpGet]
        public JsonResult getIndividualClientList()
        {
            try
            {
                var UserID = "";
                if (User.IsInRole("Admin") == true)
                {
                    UserID = null;
                }
                else
                {
                    UserID = User.Identity.GetUserId();
                }
                return Json(objIIndividualClient.getIndividualClientList(UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public JsonResult getIndividualClientListByID(int ID)
        {
            try
            {
                var EmailID= User.Identity.GetUserName();
                var UserID = User.Identity.GetUserId();
                return Json(objIIndividualClient.getIndividualClientListByID(ID, UserID, EmailID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Delete the records by id    
        [HttpGet]
        public JsonResult DeleteIndividualClientDetail(int ID)
        {
            try
            {
                var UserID = User.Identity.GetUserId();
                return Json(objIIndividualClient.DeleteIndividualClientDetail(ID, UserID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult SaveIndividualClientDetail(IndividualClientInfo IndividualClientDTO, HttpPostedFileBase LogoImageFile)
        {
            try
            {
                ApplicationDbContext context = new ApplicationDbContext();             
                var UserManager = new UserManager<AspNetUser>(new UserStore<AspNetUser>(context));
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                var UserID = User.Identity.GetUserId();
                if (LogoImageFile != null)
                {
                    string[] ext = LogoImageFile.ContentType.Split('/');
                    string extention = '.' + ext[1];
                    string[] extEmail = IndividualClientDTO.Email.Split('@');
                    string FileName = "IndividualClient" + extEmail[0] + DateTime.Now.ToString("h_mm_sstt") + extention;
                    FileName = string.Format(FileName, extention).ToString();
                    LogoImageFile.SaveAs(Server.MapPath("~/Content/img/UserImage/") + FileName);
                    IndividualClientDTO.ProfilePathImg = FileName;
                }
                var IndividualClient = objIIndividualClient.SaveIndividualClientDetail(IndividualClientDTO, UserID);
                if (IndividualClientDTO.ID != null)
                {
                    var user = UserManager.FindByEmail(IndividualClientDTO.Email);
                    user.Profile = IndividualClientDTO.ProfilePathImg;

                    if (user.Id == UserID)
                    {
                        Session["profile"] = user.Profile != null ? user.Profile : "";
                    }
                    UserManager.Update(user);
                }
                if (IndividualClient == 1)
                {

                    var user = new AspNetUser { UserName = IndividualClientDTO.Email, Email = IndividualClientDTO.Email, Profile = IndividualClientDTO.ProfilePathImg };
                   var chkUser= UserManager.Create(user, IndividualClientDTO.Password);
                    if (chkUser.Succeeded)
                    {
                        var result1 = UserManager.AddToRole(user.Id, "IndividualClient");
                        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "IndividualClient";
                        roleManager.Create(role);

                    }
                }
                if (IndividualClientDTO.Flag == "0")
                {
                    return RedirectToAction("Dashboard", "Home", new { Area = "", status = "" });
                }
                else
                {
                    return RedirectToAction("IndividualClientList", "IndividualClient", new { Area = "", status = "" });
                }

            }
            catch (Exception ex)
            {
                throw ex;
                //return RedirectToAction("AddIndividualClientList", "IndividualClient", new { Area = "", status = "N" });
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                int status;
                return status = objIIndividualClient.IsEmailExist(ID, EmailID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}