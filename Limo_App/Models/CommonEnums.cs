﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Limo_App.Models
{
    public class CommonEnums
    {    
        public enum ApiStatusCode
        {
            OK = 200,
            SERVER_ERROR = 500,
            LOGIN_FAILED = 401,
            NO_DATA = 402,
            USER_NOT_EXISTED = 403,
            PROMOTION_CODE_IS_USED = 405,
            PAYMENT_FAILURE = 406,
            USER_EXPIRED = 407,
            PROMOTION_CODE_IS_INVALID = 408
        }

        
    }

}
