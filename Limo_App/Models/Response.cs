﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using static Limo_App.Models.CommonEnums;

namespace Limo_App.Models
{
    public class Response
    {
        public int StatusCode { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public ExpandoObject Data { get; set; }

        public static Response Create(bool success, ApiStatusCode statusCode, string message = "")
        {

            return new Response
            {
                StatusCode = statusCode.GetHashCode(),
                Success = success,
                Message = !string.IsNullOrEmpty(message) ? message : statusCode.ToString(),
                Data = new ExpandoObject()
            };
        }

        public static Response Create(bool success)
        {
            return success ? Response.Succeed() : Response.Fail();
        }

        public static Response Succeed(string message = "")
        {
            return Create(true, ApiStatusCode.OK, message);
        }

        public static Response Fail(ApiStatusCode statusCode = ApiStatusCode.SERVER_ERROR, string message = "")
        {
            return Create(false, statusCode, message);
        }

        public Response AddData(string key, object value)
        {
            if (Data == null)
            {
                throw new NullReferenceException("Data");
            }

            Data.AddProperty(key, value);
            return this;
        }

        public Response AddData(object value)
        {
            Data = value.ToDynamic();
            return this;
        }
    }
}