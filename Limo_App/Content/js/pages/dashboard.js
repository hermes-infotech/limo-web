//[Dashboard Javascript]

//Project:	Qixa Admin - Responsive Admin Template
//Primary use:   Used only for the main dashboard (index.html)


$(function () {

  'use strict';
	
// Counter
	
		$('.countnm').each(function () {
			$(this).prop('Counter',0).animate({
				Counter: $(this).text()
			}, {
				duration: 5000,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	// donut chart
		$('.donut').peity('donut');
		
		// bar chart
		$(".bar").peity("bar");	
	
	// Morris-chart
	
/*	Morris.Area({
        element: 'morris-area-chart2',
        data: [{
            period: '2012',
            SiteA: 485,
            SiteB: 358,
            
        }, {
            period: '2013',
            SiteA: 359,
            SiteB: 210,
            
        }, {
            period: '2014',
            SiteA: 589,
            SiteB: 410,
            
        }, {
            period: '2015',
            SiteA: 458,
            SiteB: 344,
            
        }, {
            period: '2016',
            SiteA: 254,
            SiteB: 200,
            
        }, {
            period: '2017',
            SiteA: 754,
            SiteB: 630,
            
        },
         {
            period: '2018',
            SiteA: 845,
            SiteB: 711,
           
        }],
        xkey: 'period',
        ykeys: ['SiteA', 'SiteB'],
        labels: ['Total Booking', 'Completed Booking'],
        pointSize: 5,
        fillOpacity: 0.8,
        pointStrokeColors:['#193725', '#4f9c3a'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 3,
        smooth: true,
        hideHover: 'auto',
        lineColors: ['#193725', '#4f9c3a'],
        resize: true
        
    });
	*/
/*	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']; */

Morris.Area({
  element: 'morris-area-chart2',
  data: [
          { m: 'Jan', Corporate: 100, Bankers: 80, Individuals: 60 },
          { m: 'Feb', Corporate: 200, Bankers: 190, Individuals: 160 },
          { m: 'Mar', Corporate: 175, Bankers: 140, Individuals: 120 },
          { m: 'Apr', Corporate: 350, Bankers: 250, Individuals: 180 },
          { m: 'May', Corporate: 400, Bankers: 320, Individuals: 160 },
          { m: 'Jun', Corporate: 50, Bankers: 40, Individuals: 20 },
          { m: 'Jul', Corporate: 70, Bankers: 65, Individuals: 30 },
          { m: 'Aug', Corporate: 160, Bankers: 140, Individuals: 120 },
          { m: 'Sep', Corporate: 180, Bankers: 150, Individuals: 50 },
          { m: 'Oct', Corporate: 260, Bankers: 230, Individuals: 80 },
          { m: 'Nov', Corporate: 450, Bankers: 380, Individuals: 90 },
          { m: 'Dec', Corporate: 420, Bankers: 370, Individuals: 360 },
        ],
  xkey: 'm',
parseTime: false,
  ykeys: ['Corporate', 'Bankers', 'Individuals'],
  labels: ['Corporate', 'Bankers', 'Individuals'],
	pointSize: 5,
	fillOpacity: 0.8,
        pointStrokeColors:['#193725', '#4f9c3a', '#A2C037'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 3,
        smooth: true,
        hideHover: 'auto',
        lineColors: ['#193725', '#4f9c3a', '#A2C037'],
        resize: true,
});
	
	//BAR CHART

	var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
        {y: 'Jan', a: 5, b: 5, c: 6},
        {y: 'Feb', a: 1, b: 2, c: 3},
        {y: 'Mar', a: 7, b: 5, c: 3},
        {y: 'Apr', a: 1, b: 2, c: 5},
        {y: 'May', a: 9, b: 5, c: 9},
        {y: 'Jun', a: 10, b: 5, c: 6},
		{y: 'Jul', a: 5, b: 3, c: 7}
      ],
		barColors: ['#193725', '#4f9c3a', '#A2C037'],
		barSizeRatio: 0.5,
		barGap:5,
		xkey: 'y',
		ykeys: ['a', 'b', 'c'],
		labels: ['Corporate', 'Bankers', 'Individuals'],
		hideHover: 'auto'
    });
	
	//sparkline
		$("#barchart4").sparkline([32,24,26,24,32,26,40,34,22,24], {
			type: 'bar',
			height: '120',
			width: '65%',
			barWidth: 8,
			barSpacing: 4,
			barColor: '#ffffff',
		});
		$("#linearea3").sparkline([32,24,26,24,32,26,40,34,22,24], {
			type: 'line',
			width: '50%',
			height: '120',
			lineColor: '#ffffff',
			fillColor: '#ffffff',
			lineWidth: 1,
		});
	
		



}); // End of use strict

