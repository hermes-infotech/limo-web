﻿function validate() {
    var isValid = true;
    if ($('#txtName').val() === "") {
        $('#txtName').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtName').css('border-color', 'lightgrey');
    }
    
    if ($('#txtPassword').val() === "") {
        $('#txtPassword').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtPassword').css('border-color', 'lightgrey');
    }
    if ($('#txtPhone').val() === "") {

        $('#txtPhone').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtPhone').css('border-color', 'lightgrey');
        var value = $('#txtPhone').val();
        if (value != null && value != "" && value != undefined) {
            if (value.length >= 8 && value.length < 16) {

            }
            else {
                alert("Phone Number must be between 8-15 Digits");
                $('#txtPhone').css('border-color', 'Red');
                isValid = false;
            }
        }
        else {
            $('#txtPhone').css('border-color', 'Red');
            isValid = false;
        }
    }
    //if ($('#txtPhone').val() === "") {
    //    $('#txtPhone').css('border-color', 'Red');
    //    isValid = false;
    //}
    //else {
    //    $('#txtPhone').css('border-color', 'lightgrey');
    //}
    if ($('#txtEmail').val() === "") {
        $('#txtEmail').css('border-color', 'Red');
        isValid = false;
    }
    else {
        if (validateEmail($('#txtEmail').val()) === true) {
            $('#txtEmail').css('border-color', 'lightgrey');
        } else {
            alert("Email format is not correct");
            $('#txtEmail').css('border-color', 'Red');
            isValid = false;
        }

    }
    if ($('#ddlState').val() === "") {
        $('#ddlState').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#ddlState').css('border-color', 'lightgrey');
    }
    if ($('#ddlCity').val() === "") {
        $('#ddlCity').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#ddlCity').css('border-color', 'lightgrey');
    }
    if ($('#txtZipcode').val() === "") {
        $('#txtZipcode').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtZipcode').css('border-color', 'lightgrey');
    }
    if ($('#txtAddress1').val() === "") {
        $('#txtAddress1').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtAddress1').css('border-color', 'lightgrey');
    }
    if ($('#logoName').val() === '' || $('#logoName').val() === null || $('#logoName').val() === undefined) {
        if ($('#uploadLogo').val() === "") {
            $('#uploadLogo').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $('#uploadLogo').css('border-color', 'lightgrey');
        }
    }
    if ($('#txtLicenseNo').val() !== undefined) {
        if ($('#txtLicenseNo').val() === "") {
            $('#txtLicenseNo').css('border-color', 'Red');
            isValid = false;
        }
        else {
            $('#txtLicenseNo').css('border-color', 'lightgrey');
        }
    }

    if ($('#txtDOB').val() === "") {
        $('#txtDOB').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtDOB').css('border-color', 'lightgrey');
    }
    return isValid;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function formatDate_dd_mm_yyyy(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
}


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
}

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}
function splitTextboxString(txtValue) {
    //txtValue = txtValue.replace("'", "''")
    txtValue = txtValue.trim();
    return txtValue;
}

function validateLogin() {
    var isValid = true;
    if ($('#txtUserName').val() === "") {
        $('#txtUserName').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtUserName').css('border-color', 'lightgrey');
    }
    if ($('#txtPassword').val() === "") {
        $('#txtPassword').css('border-color', 'Red');
        isValid = false;
    }
    else {
        $('#txtPassword').css('border-color', 'lightgrey');
    }
    return isValid;
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function showImage() {
    var src = document.getElementById("uploadLogo");
    var target = document.getElementById("ProfilePic");
    $('#ProfilePic').show();
    var fr = new FileReader();
    fr.onload = function () {  target.src = fr.result; }
    fr.readAsDataURL(src.files[0]);

}