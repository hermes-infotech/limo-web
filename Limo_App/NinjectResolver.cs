﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using Ninject;
using System.Web.Http.Dependencies;
using Ninject.Extensions.ChildKernel;

using Limo_BusinessLayer.Components;
using Limo_BusinessLayer.Interfaces;
using Limo_DataLayer.Components;
using Limo_DataLayer.Interfaces;
namespace Limo_App
{
    public class NinjectResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectResolver() : this(new StandardKernel())
        {
        }

        public NinjectResolver(IKernel ninjectKernel, bool scope = false)
        {
            kernel = ninjectKernel;
            if (!scope)
            {
                AddBindings(kernel);
            }
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectResolver(AddRequestBindings(new ChildKernel(kernel)), true);
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        public void Dispose()
        {

        }

        private void AddBindings(IKernel kernel)
        {
            // singleton and transient bindings go here
        }

        private IKernel AddRequestBindings(IKernel kernel)
        {
            kernel.Bind<IIndividualClientBAL>().To<IndividualClientBAL>().InSingletonScope();
            kernel.Bind<IIndividualClientDAL>().To<IndividualClientDAL>().InSingletonScope();
            kernel.Bind<ICorporateClientBAL>().To<CorporateClientBAL>().InSingletonScope();
            kernel.Bind<ICorporateClientDAL>().To<CorporateClientDAL>().InSingletonScope();
            kernel.Bind<IDriverBAL>().To<DriverBAL>().InSingletonScope();
            kernel.Bind<IDriverDAL>().To<DriverDAL>().InSingletonScope();
            return kernel;
        }
    }
}