﻿using Limo_BusinessLayer.Interfaces;
using Limo_DataLayer.Interfaces;
using Limo_DTO.CorporateClient;
using Limo_DTO.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_BusinessLayer.Components
{
    public class DriverBAL : IDriverBAL
    {
        private readonly IDriverDAL _IDriverDAL;
        public DriverBAL(IDriverDAL IDriverDAL)
        {
            this._IDriverDAL = IDriverDAL;
        }
        public List<DriverInfo> getDriverList(string UserID)
        {
            try
            {
                return _IDriverDAL.getDriverList(UserID);
            }
            catch
            {
                throw;
            }
        }
        public DriverInfo getDriverListByID(int ID, string UserID, string EmailID)
        {
            try
            {
                return _IDriverDAL.getDriverListByID(ID, UserID, EmailID);
            }
            catch
            {
                throw;
            }
        }
        public string DeleteDriverDetail(int ID, string UserID)
        {
            try
            {
                return _IDriverDAL.DeleteDriverDetail(ID, UserID);
            }
            catch
            {
                throw;
            }
        }
        public int SaveDriverDetail(DriverInfo DriverDTO, string UserID)
        {
            try
            {
                return _IDriverDAL.SaveDriverDetail(DriverDTO,UserID);
            }
            catch
            {
                throw;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                return _IDriverDAL.IsEmailExist(ID, EmailID);
            }
            catch
            {
                throw;
            }
        }
        public string DriverAction(DriverJobActionModel djModel, string userId)
        {
            var x = _IDriverDAL.DriverAction(djModel, userId);
            return x;
        }
        public List<JobListModel> GetDriverJobList(int driverId)
        {
            try
            {
                return _IDriverDAL.GetDriverJobList(driverId);
            }
            catch
            {
                throw;
            }
        }

        public int SaveDriverRating(DriverRatingModel drModel)
        {
            var x = _IDriverDAL.SaveDriverRating(drModel);
            return x;
        }
    }
}
