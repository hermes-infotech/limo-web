﻿using Limo_BusinessLayer.Interfaces;
using Limo_DataLayer.Interfaces;
using Limo_DTO.CorporateClient;
using System;
using System.Collections.Generic;

namespace Limo_BusinessLayer.Components
{
    public class CorporateClientBAL: ICorporateClientBAL
    {
        private readonly ICorporateClientDAL _ICorporateClientDAL;
        
        public CorporateClientBAL(ICorporateClientDAL ICorporateClientDAL)
        {
            this._ICorporateClientDAL = ICorporateClientDAL;
        }
        public List<CorporateClientInfo> getCorporateClientList(string UserID)
        {
            try
            {
                return _ICorporateClientDAL.getCorporateClientList(UserID);
            }
            catch
            {
                throw;
            }
        }
        public CorporateClientInfo getCorporateClientListByID(int ID, string UserID,string EmailID)
        {
            try
            {
                return _ICorporateClientDAL.getCorporateClientListByID(ID, UserID, EmailID);
            }
            catch
            {
                throw;
            }
        }
        public string DeleteCorporateClientDetail(int ID, string UserID)
        {
            try
            {
                return _ICorporateClientDAL.DeleteCorporateClientDetail(ID, UserID);
            }
            catch
            {
                throw;
            }
        }
        public int SaveCorporateClientDetail(CorporateClientInfo CorporateClientDTO, string UserID)
        {
            try
            {
                return _ICorporateClientDAL.SaveCorporateClientDetail(CorporateClientDTO, UserID);
            }
            catch
            {
                throw;
            }
        }
        public int SaveBookingDetail(BookingInfo BookingInfoDTO, string UserID)
        {
            try
            {
                return _ICorporateClientDAL.SaveBookingDetail(BookingInfoDTO, UserID);
            }
            catch
            {
                throw;
            }
        }
        public List<BookingInfo> getBookingList(string UserID)
        {
            try
            {
                return _ICorporateClientDAL.getBookingList(UserID);
            }
            catch
            {
                throw;
            }
        }
        public List<BookingInfo> getBookingListForApi(string Email)
        {
            try
            {
                return _ICorporateClientDAL.getBookingListForApi(Email);
            }
            catch
            {
                throw;
            }
        }
        public BookingInfo getBookingListByID(int ID, string UserID)
        {
            try
            {
                return _ICorporateClientDAL.getBookingListByID(ID,UserID);
            }
            catch
            {
                throw;
            }
        }
        public string DeleteBookingDetail(int ID, string UserID)
        {
            try
            {
                return _ICorporateClientDAL.DeleteBookingDetail(ID, UserID);
            }
            catch
            {
                throw;
            }
        }
        public List<JobListModel> getJobList(string UserID)
        {
            try
            {
                return _ICorporateClientDAL.getJobList(UserID);
            }
            catch
            {
                throw;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                return _ICorporateClientDAL.IsEmailExist(ID, EmailID);
            }
            catch
            {
                throw;
            }
        }
    }
}
