﻿using Limo_BusinessLayer.Interfaces;
using Limo_DataLayer.Interfaces;
using Limo_DTO.IndividualClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_BusinessLayer.Components
{
    public class IndividualClientBAL: IIndividualClientBAL
    {
        private readonly IIndividualClientDAL _IIndividualClientDAL;
        public IndividualClientBAL(IIndividualClientDAL IIndividualClientDAL)
        {
            this._IIndividualClientDAL = IIndividualClientDAL;
        }
        public List<IndividualClientInfo> getIndividualClientList(string UserID)
        {
            try
            {
                return _IIndividualClientDAL.getIndividualClientList(UserID);
            }
            catch
            {
                throw;
            }
        }
        public IndividualClientInfo getIndividualClientListByID(int ID, string UserID,string EmailID)
        {
            try
            {
                return _IIndividualClientDAL.getIndividualClientListByID(ID,UserID, EmailID);
            }
            catch
            {
                throw;
            }
        }
        public string DeleteIndividualClientDetail(int ID, string UserID)
        {
            try
            {
                return _IIndividualClientDAL.DeleteIndividualClientDetail(ID, UserID);
            }
            catch
            {
                throw;
            }
        }
        public int SaveIndividualClientDetail(IndividualClientInfo IndividualClientDTO, string UserID)
        {
            try
            {
                return _IIndividualClientDAL.SaveIndividualClientDetail(IndividualClientDTO, UserID);
            }
            catch
            {
                throw;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                return _IIndividualClientDAL.IsEmailExist(ID, EmailID);
            }
            catch
            {
                throw;
            }
        }
    }
}
