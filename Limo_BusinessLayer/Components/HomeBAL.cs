﻿using Limo_BAL.Interfaces;
using Limo_DAL.Interfaces;
using Limo_DTO.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo.BAL.Components
{
    public class HomeBAL : IHomeBAL
    {
        private readonly IHomeDAL _IHomeDAL;
        public HomeBAL(IHomeDAL IHomeDAL)
        {
            this._IHomeDAL = IHomeDAL;
        }
        public List<DriverInfo> getDriverList()
        {
            try
            {
                return _IHomeDAL.getDriverList();
            }
            catch
            {
                throw;
            }
        }

        public DriverInfo DriverDetailsByID(int ID)
        {
            try
            {
                return _IHomeDAL.DriverDetailsByID(ID);
            }
            catch
            {
                throw;
            }
        }
        public int SaveDriverDetail(DriverInfo DriverDTO)
        {
            try
            {
                return _IHomeDAL.SaveDriverDetail(DriverDTO);
            }
            catch
            {
                throw;
            }
        }
        public string DeleteDriverDetail(int ID)
        {
            try
            {
                return _IHomeDAL.DeleteDriverDetail(ID);
            }
            catch
            {
                throw;
            }
        }
        //-----------------------For other users-------------------------
        public List<UserInfo> getUserList(int UpdatedBy)
        {
            try
            {
                return _IHomeDAL.getUserList(UpdatedBy);
            }
            catch
            {
                throw;
            }
        }

        public List<UserInfo> getUserListForUser(int TypeID,int AddedBy)
        {
            try
            {
                return _IHomeDAL.getUserListForUser(TypeID,AddedBy);
            }
            catch
            {
                throw;
            }
        }
        public UserInfo UserDetailsByID(int ID, int TypeID)
        {
            try
            {
                return _IHomeDAL.UserDetailsByID(ID,TypeID);
            }
            catch
            {
                throw;
            }
        }
        public int SaveUserDetail(UserInfo UserDTO)
        {
            try
            {
                return _IHomeDAL.SaveUserDetail(UserDTO);
            }
            catch
            {
                throw;
            }
        }
        public string DeleteUserDetail(int ID,int TypeID)
        {
            try
            {
                return _IHomeDAL.DeleteUserDetail(ID,TypeID);
            }
            catch
            {
                throw;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                return _IHomeDAL.IsEmailExist(ID, EmailID);
            }
            catch
            {
                throw;
            }
        }
    }
}
