﻿using Limo_BusinessLayer.Components;
using Limo_BusinessLayer.Interfaces;
using Limo_DataLayer.Components;
using Limo_DataLayer.Interfaces;
using Ninject;
namespace Limo_BAL
{
    public class ConfigureNInjectServices
    {
        public static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IIndividualClientBAL>().To<IndividualClientBAL>();
            kernel.Bind<IIndividualClientDAL>().To<IndividualClientDAL>();
            kernel.Bind<ICorporateClientBAL>().To<CorporateClientBAL>();
            kernel.Bind<ICorporateClientDAL>().To<CorporateClientDAL>();
            kernel.Bind<IDriverBAL>().To<DriverBAL>();
            kernel.Bind<IDriverDAL>().To<DriverDAL>();
        }
    }
}