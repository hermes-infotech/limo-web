﻿using Limo_DTO.CorporateClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_BusinessLayer.Interfaces
{
    public interface ICorporateClientBAL
    {
        List<CorporateClientInfo> getCorporateClientList(string UserID);
        CorporateClientInfo getCorporateClientListByID(int ID, string UserID, string EmailID);
        string DeleteCorporateClientDetail(int ID, string UserID);
        int SaveCorporateClientDetail(CorporateClientInfo CorporateClientDTO, string UserID);
        int SaveBookingDetail(BookingInfo BookingInfoDTO, string UserID);
        List<BookingInfo> getBookingList(string UserID);
        List<BookingInfo> getBookingListForApi(string Email);
        BookingInfo getBookingListByID(int ID, string UserID);
        string DeleteBookingDetail(int ID, string UserID);
        int IsEmailExist(int? ID, string EmailID);
        List<JobListModel> getJobList(string UserID);
    }
}
