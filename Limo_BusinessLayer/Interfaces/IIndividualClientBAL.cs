﻿using Limo_DTO.IndividualClient;
using System.Collections.Generic;

namespace Limo_BusinessLayer.Interfaces
{
    public interface IIndividualClientBAL
    {
        List<IndividualClientInfo> getIndividualClientList(string UserID);
        IndividualClientInfo getIndividualClientListByID(int ID, string UserID,string EmailID);
        string DeleteIndividualClientDetail(int ID, string UserID);
        int SaveIndividualClientDetail(IndividualClientInfo IndividualClientDTO, string UserID);
        int IsEmailExist(int? ID, string EmailID);
    }
}
