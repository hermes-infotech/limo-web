﻿using Limo_DTO.CorporateClient;
using Limo_DTO.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_BusinessLayer.Interfaces
{
    public interface IDriverBAL
    {
        List<DriverInfo> getDriverList(string UserID);
        DriverInfo getDriverListByID(int ID, string UserID, string EmailID);
        string DeleteDriverDetail(int ID, string UserID);
        int SaveDriverDetail(DriverInfo DriverDTO, string UserID);
        int IsEmailExist(int? ID, string EmailID);
        string DriverAction(DriverJobActionModel djModel, string UserId);
        List<JobListModel> GetDriverJobList(int driverid);
        int SaveDriverRating(DriverRatingModel drModel);
    }
}
