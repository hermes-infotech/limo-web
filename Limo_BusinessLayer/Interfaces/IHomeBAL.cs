﻿using Limo_DTO.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_BAL.Interfaces
{
    public interface IHomeBAL
    {
        List<DriverInfo> getDriverList();
        DriverInfo DriverDetailsByID(int ID);
        string DeleteDriverDetail(int ID);
        int SaveDriverDetail(DriverInfo DriverDTO);
        List<UserInfo> getUserList(int TypeID);
        List<UserInfo> getUserListForUser(int TypeID,int AddedBy);
        UserInfo UserDetailsByID(int ID, int TypeID);
        string DeleteUserDetail(int ID, int TypeID);
        int SaveUserDetail(UserInfo UserDTO);
        int IsEmailExist(int? ID, string EmailID);
    }
}
