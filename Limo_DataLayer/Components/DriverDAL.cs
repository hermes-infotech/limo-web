﻿using Limo_DataLayer.Entity;
using Limo_DataLayer.Interfaces;
using Limo_DTO.CorporateClient;
using Limo_DTO.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DataLayer.Components
{
    public class DriverDAL : IDriverDAL
    {
        public List<DriverInfo> getDriverList(string UserID)
        {
            try
            {
                List<DriverInfo> DriverList = new List<DriverInfo>();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    DriverInfo objDriverInfo = new DriverInfo();
                    var objDriverList = LImOEntitiesdb.GetDriverList(UserID).ToList();
                    foreach (var DriverDetails in objDriverList)
                    {

                        objDriverInfo = new DriverInfo();
                        objDriverInfo.ID = DriverDetails.ID;
                        objDriverInfo.DriverLicense = DriverDetails.DriverLicense;
                        objDriverInfo.Status = DriverDetails.Status == true ? "Active" : "Locked";
                        objDriverInfo.FirstName = DriverDetails.FirstName;
                        objDriverInfo.Phone = DriverDetails.Phone;
                        //objDriverInfo.DOB = Convert.ToDateTime((DriverDetails.DOB).ToString("yyyy-MM-dd"));
                        objDriverInfo.Email = DriverDetails.Email;
                        //objDriverInfo.DriverLicense = DriverDetails.DriverLicense;
                        objDriverInfo.ProfilePathImg = DriverDetails.ProfilePathImg;
                        DriverList.Add(objDriverInfo);
                    }
                }
                return DriverList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DriverInfo getDriverListByID(int ID, string UserID, string EmailID)
        {
            try
            {
                DriverInfo objDriverInfo = new DriverInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    objDriverInfo = LImOEntitiesdb.GetDriverDetailByID(ID, UserID, EmailID).Select(s => new DriverInfo
                    {
                        ID = s.ID,
                        FirstName = s.FirstName,
                        LastName = s.LastName,
                        Password = s.Password,
                        Phone = s.Phone,
                        Address1 = s.Address1,
                        Address2 = s.Address2,
                        CountryId = s.CountryId,
                        StateId = s.StateId,
                        CityId = s.CityId,
                        Email = s.Email,
                        Gender = s.Gender,
                        ProfilePathImg = s.ProfilePathImg,
                        PinCode = s.PinCode,
                        DateOfBirth = s.DOB,
                        Rating=s.Rating.HasValue?s.Rating.Value:0.0,
                        TotolCompletedRides=s.TotalJobs.HasValue?s.TotalJobs.Value:0
                    }).FirstOrDefault();

                    //var DriverDetails = LImOEntitiesdb.GetDriverDetailByID(ID,UserID,EmailID).ToList();
                    //foreach (var DriverDetail in DriverDetails)
                    //{
                    //    objDriverInfo.ID = DriverDetail.ID;
                    //    objDriverInfo.FirstName = DriverDetail.FirstName;
                    //    objDriverInfo.LastName = DriverDetail.LastName;
                    //    objDriverInfo.Password = DriverDetail.Password;
                    //    objDriverInfo.Phone = DriverDetail.Phone;
                    //    objDriverInfo.Address1 = DriverDetail.Address1;
                    //    objDriverInfo.Address2 = DriverDetail.Address2;
                    //    objDriverInfo.CountryId = DriverDetail.CountryId;
                    //    objDriverInfo.StateId = DriverDetail.StateId;
                    //    objDriverInfo.CityId = DriverDetail.CityId;
                    //    objDriverInfo.Email = DriverDetail.Email;
                    //    objDriverInfo.Gender = DriverDetail.Gender;
                    //    objDriverInfo.ProfilePathImg = DriverDetail.ProfilePathImg;
                    //    objDriverInfo.PinCode = DriverDetail.PinCode;
                    //    objDriverInfo.DOB = Convert.ToDateTime(DriverDetail.DOB).ToString("yyyy-MM-dd");

                    //}
                }
                return objDriverInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteDriverDetail(int ID, string UserID)
        {
            try
            {
                string deleteStatus;
                DriverInfo objDriverInfo = new DriverInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    var DriverDetails = LImOEntitiesdb.DeleteDriverDetail(ID, UserID);
                    deleteStatus = DriverDetails.ToString();
                }
                return deleteStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveDriverDetail(DriverInfo DriverDTO, string UserID)
        {
            try
            {
                DriverInfo objDriverInfo = new DriverInfo();

                using (Entities LImOEntitiesdb = new Entities())
                {
                    if (DriverDTO.ID == 0)
                    {
                        DriverDTO.ID = null;
                    }
                    var DriverDetails = LImOEntitiesdb.SaveDriverDetail(DriverDTO.ID, DriverDTO.FirstName, DriverDTO.LastName, DriverDTO.Password, DriverDTO.Email, DriverDTO.DriverLicense, DriverDTO.Phone, DriverDTO.Gender, Convert.ToDateTime(DriverDTO.DOB), DriverDTO.ProfilePathImg, DriverDTO.Address1, DriverDTO.Address2, DriverDTO.CountryId, DriverDTO.StateId, DriverDTO.CityId, DriverDTO.PinCode, DriverDTO.DriverLicenseFront, DriverDTO.DriverLicenseBack, UserID);
                    int? state = null;
                    foreach (var m in DriverDetails)
                    {
                        state = m;
                    }
                    return Convert.ToInt32(state);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                int Status;
                using (Entities LImOEntitiesdb = new Entities())
                {
                    if (ID == null)
                    {
                        var temp = LImOEntitiesdb.DriverDetails.ToList().Where(x => x.Email == EmailID);
                        Status = temp.Count();
                    }
                    else
                    {
                        Status = LImOEntitiesdb.DriverDetails.ToList().Where(x => x.Email == EmailID && x.ID == ID).Count();
                    }
                }
                return Status;
            }
            catch
            {
                throw;
            }
        }
        public string DriverAction(DriverJobActionModel djModel, string userId)
        {
            string driverAction = string.Empty;
            try
            {
                using (Entities LImOEntitiesdb = new Entities())
                {
                    djModel.UserId = userId;
                    var status = LImOEntitiesdb.DriverJobAction(djModel.DriverId, djModel.JobId, djModel.UserId, (int)djModel.Action);
                    int? state = null;
                    foreach (var m in status)
                    {
                        state = m;
                    }
                    driverAction = Convert.ToString((EnumJobAction)state);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return driverAction;
        }
        public List<JobListModel> GetDriverJobList(int UserID)
        {
            try
            {
                List<JobListModel> jobList = new List<JobListModel>();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    JobListModel objJobListInfo = new JobListModel();
                    jobList = LImOEntitiesdb.GetDriverJobList(UserID).
                        Select(s => new JobListModel
                        {
                            BookingId = s.BookingId.ToString(),
                            ClientName = s.PassengerName,
                            DriverFirstName = s.firstname,
                            DriverLastName = s.lastname,
                            DateAndTime = s.date,
                            JobStatus = s.jobStatus,
                            Car = s.car,
                            PickUpTime = s.pickuptime,
                            PickUpLocation = !string.IsNullOrEmpty(s.PickupLocation) ? s.PickupLocation : string.Empty,
                            DestinationLocation = !string.IsNullOrEmpty(s.destinationlocation) ? s.destinationlocation : string.Empty
                        }).ToList();
                }
                return jobList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveDriverRating(DriverRatingModel drModel)
        {
            try
            {
                using (Entities LImOEntitiesdb = new Entities())
                {
                    // djModel.UserId = userId;
                    string mode = "insert";
                    var status = LImOEntitiesdb.DriverRatingProc(0, drModel.DriverId, drModel.JobId, drModel.CustomerSignature, drModel.Rating, mode);
                    int? state = null;
                    foreach (var m in status)
                    {
                        state = m;
                    }
                    return state.HasValue ? state.Value : 0;
                }
            }
            catch
            {
                throw;
            }

        }
    }
}
