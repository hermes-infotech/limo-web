﻿using Limo_DataLayer.Entity;
using Limo_DataLayer.Interfaces;
using Limo_DTO.IndividualClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DataLayer.Components
{
    public class IndividualClientDAL: IIndividualClientDAL
    {
        public List<IndividualClientInfo> getIndividualClientList(string UserID)
        {
            try
            {
                List<IndividualClientInfo> IndividualClientList = new List<IndividualClientInfo>();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    IndividualClientInfo objIndividualClientInfo = new IndividualClientInfo();
                    var objIndividualClientList = LImOEntitiesdb.GetIndividualClientList(UserID).ToList();
                    foreach (var IndividualClientDetails in objIndividualClientList)
                    {
                        objIndividualClientInfo = new IndividualClientInfo();
                        objIndividualClientInfo.ID = IndividualClientDetails.ID;
                        objIndividualClientInfo.FirstName = IndividualClientDetails.FirstName;
                        objIndividualClientInfo.Phone = IndividualClientDetails.Phone;
                        //objIndividualClientInfo.DOB = Convert.ToDateTime((IndividualClientDetails.DOB).ToString("yyyy-MM-dd"));
                        objIndividualClientInfo.Email = IndividualClientDetails.Email;
                        //objIndividualClientInfo.IndividualClientLicense = IndividualClientDetails.IndividualClientLicense;
                        objIndividualClientInfo.ProfilePathImg = IndividualClientDetails.ProfilePathImg;
                        IndividualClientList.Add(objIndividualClientInfo);
                    }
                }
                return IndividualClientList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IndividualClientInfo getIndividualClientListByID(int ID, string UserID, string EmailID)
        {
            try
            {
                IndividualClientInfo objIndividualClientInfo = new IndividualClientInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    var IndividualClientDetails = LImOEntitiesdb.GetIndividualClientDetailByID(ID, UserID, EmailID).ToList();
                    foreach (var IndividualClientDetail in IndividualClientDetails)
                    {
                        objIndividualClientInfo.ID = IndividualClientDetail.ID;
                        objIndividualClientInfo.FirstName = IndividualClientDetail.FirstName;
                        objIndividualClientInfo.LastName = IndividualClientDetail.LastName;
                        objIndividualClientInfo.Password = IndividualClientDetail.Password;
                        objIndividualClientInfo.Phone = IndividualClientDetail.Phone;
                        objIndividualClientInfo.Address1 = IndividualClientDetail.Address1;
                        objIndividualClientInfo.Address2 = IndividualClientDetail.Address2;
                        objIndividualClientInfo.CountryId = IndividualClientDetail.CountryId;
                        objIndividualClientInfo.StateId = IndividualClientDetail.StateId;
                        objIndividualClientInfo.CityId = IndividualClientDetail.CityId;
                        objIndividualClientInfo.Email = IndividualClientDetail.Email;
                        objIndividualClientInfo.Gender = IndividualClientDetail.Gender;
                        objIndividualClientInfo.ProfilePathImg = IndividualClientDetail.ProfilePathImg;
                        objIndividualClientInfo.PinCode = IndividualClientDetail.PinCode;
                        objIndividualClientInfo.DOB = Convert.ToDateTime(IndividualClientDetail.DOB).ToString("yyyy-MM-dd");

                    }
                }
                return objIndividualClientInfo;
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteIndividualClientDetail(int ID, string UserID)
        {
            try
            {
                string deleteStatus;
                IndividualClientInfo objIndividualClientInfo = new IndividualClientInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    var IndividualClientDetails = LImOEntitiesdb.DeleteIndividualClient(ID, UserID);
                    deleteStatus = IndividualClientDetails.ToString();
                }
                return deleteStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveIndividualClientDetail(IndividualClientInfo IndividualClientDTO, string UserID)
        {
            try
            {
                IndividualClientInfo objIndividualClientInfo = new IndividualClientInfo();

                using (Entities LImOEntitiesdb = new Entities())
                {
                    if (IndividualClientDTO.ID == 0)
                    {
                        IndividualClientDTO.ID = null;
                    }
                    var IndividualClientDetails = LImOEntitiesdb.SaveIndividualClientDetail(IndividualClientDTO.ID, IndividualClientDTO.FirstName, IndividualClientDTO.LastName, IndividualClientDTO.Password, IndividualClientDTO.Email, IndividualClientDTO.Phone, IndividualClientDTO.Gender, null, IndividualClientDTO.ProfilePathImg, IndividualClientDTO.Address1, IndividualClientDTO.Address2, IndividualClientDTO.CountryId, IndividualClientDTO.StateId, IndividualClientDTO.CityId, IndividualClientDTO.PinCode, UserID);
                    int? state = null;
                    foreach (var m in IndividualClientDetails)
                    {
                        state = m;
                    }
                    return Convert.ToInt32(state);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                int Status;
                using (Entities LImOEntitiesdb = new Entities())
                {
                    if (ID == null)
                    {
                        var temp = LImOEntitiesdb.IndividualClients.ToList().Where(x => x.Email == EmailID);
                        Status = temp.Count();
                    }
                    else
                    {
                        Status = LImOEntitiesdb.IndividualClients.ToList().Where(x => x.Email == EmailID && x.ID == ID).Count();
                    }
                }
                return Status;
            }
            catch
            {
                throw;
            }
        }
    }
}
