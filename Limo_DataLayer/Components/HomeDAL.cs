﻿using Limo_DAL.Interfaces;
using Limo_DTO.Home;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Limo_DAL.Components
{
    public class HomeDAL:IHomeDAL
    {
        public List<DriverInfo> getDriverList()
        {
            return null;
            //try
            //{
            //    List<DriverInfo> DriverList = new List<DriverInfo>();
            //    using (Entities LImOEntitiesdb = new Entities())
            //    {
            //        DriverInfo objDriverInfo = new DriverInfo();
            //        var objDriverList = LImOEntitiesdb.GetDriverList().ToList();
            //        foreach (var DriverDetails in objDriverList)
            //        {
            //            objDriverInfo = new DriverInfo();
            //            objDriverInfo.ID = DriverDetails.ID;
            //            objDriverInfo.FirstName = DriverDetails.FirstName;
            //            objDriverInfo.Phone = DriverDetails.Phone;
            //            objDriverInfo.DOB = Convert.ToDateTime(DriverDetails.DOB).ToString("yyyy-MM-dd");
            //            objDriverInfo.Email = DriverDetails.Email;
            //            //objDriverInfo.DriverLicense = DriverDetails.DriverLicense;
            //            objDriverInfo.ProfilePathImg = DriverDetails.ProfilePathImg;
            //            DriverList.Add(objDriverInfo);
            //        }
            //    }
            //    return DriverList;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
        public DriverInfo DriverDetailsByID(int ID)
        {
            return null;
            //try
            //{
            //    DriverInfo objDriverInfo = new DriverInfo();
            //    using (Entities LImOEntitiesdb = new Entities())
            //    {
            //        var DriverDetails = LImOEntitiesdb.GetDriverDetailBy(ID).ToList();
            //        foreach (var DriverDetail in DriverDetails)
            //        {
            //            objDriverInfo.ID = DriverDetail.ID;
            //            objDriverInfo.FirstName = DriverDetail.FirstName;
            //            objDriverInfo.LastName = DriverDetail.LastName;
            //            objDriverInfo.UserName = DriverDetail.UserName;
            //            objDriverInfo.Password = DriverDetail.Password;
            //            objDriverInfo.Phone = DriverDetail.Phone;
            //            objDriverInfo.Address1 = DriverDetail.Address1;
            //            objDriverInfo.Address2 = DriverDetail.Address2;
            //            objDriverInfo.CountryId = DriverDetail.CountryId;
            //            objDriverInfo.StateId = DriverDetail.StateId;
            //            objDriverInfo.CityId = DriverDetail.CityId;
            //            objDriverInfo.Email = DriverDetail.Email;
            //            objDriverInfo.Gender = DriverDetail.Gender;
            //            objDriverInfo.ProfilePathImg = DriverDetail.ProfilePathImg;
            //            objDriverInfo.PinCode = DriverDetail.PinCode;
            //            objDriverInfo.DOB = Convert.ToDateTime(DriverDetail.DOB).ToString("yyyy-MM-dd");
            //            objDriverInfo.DriverLicense = DriverDetail.DriverLicense;

            //        }
            //    }
            //    return objDriverInfo;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
        public int SaveDriverDetail(DriverInfo DriverDTO)
        {
            return 3;
            //try
            //{
            //    DriverInfo objDriverInfo = new DriverInfo();

            //    using (Entities LImOEntitiesdb = new Entities())
            //    {
            //        var DriverDetails = LImOEntitiesdb.SaveDriverDetail(DriverDTO.ID, DriverDTO.FirstName, DriverDTO.LastName, DriverDTO.UserName, DriverDTO.Password, DriverDTO.Email,DriverDTO.DriverLicense, DriverDTO.Phone, DriverDTO.Gender, Convert.ToDateTime(DriverDTO.DOB), DriverDTO.ProfilePathImg, DriverDTO.Address1, DriverDTO.Address2, DriverDTO.CountryId, DriverDTO.StateId, DriverDTO.CityId, DriverDTO.PinCode);
            //        //int aa = Convert.ToInt32(DriverDetails[0]);
            //        int? state = null;
            //        foreach (var m in DriverDetails)
            //        {
            //            state = m;
            //        }
            //        return Convert.ToInt32(state);
            //    }


            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
        public string DeleteDriverDetail(int ID)
        {
            return null;
            //try
            //{
            //    string deleteStatus;
            //    DriverInfo objDriverInfo = new DriverInfo();
            //    using (Entities LImOEntitiesdb = new Entities())
            //    {
            //        var DriverDetails = LImOEntitiesdb.DeleteDriverDetail(ID);
            //        deleteStatus=DriverDetails.ToString();
            //    }
            //    return deleteStatus;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
        //-----------For other users(Client/Jobs etc)------------------------------------------
        public List<UserInfo> getUserList(int UpdatedBy)
        {
            return null;
        }

        public List<UserInfo> getUserListForUser(int TypeID,int AddedBy)
        {
            return null;
        }
        public UserInfo UserDetailsByID(int ID,int TypeID)
        {
            return null;
        }
        public int SaveUserDetail(UserInfo UserDTO)
        {
            return 9;
        }
        public string DeleteUserDetail(int ID,int TypeID)
        {
            return null;
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            return 0;
        }
    }
}
