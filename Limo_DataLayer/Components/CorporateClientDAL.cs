﻿using Limo_DataLayer.Entity;
using Limo_DataLayer.Interfaces;
using Limo_DTO.CorporateClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DataLayer.Components
{
    public class CorporateClientDAL : ICorporateClientDAL
    {
        private Entities LImOEntitiesdb = new Entities();

        public List<CorporateClientInfo> getCorporateClientList(string UserID)
        {
            try
            {
                List<CorporateClientInfo> CorporateClientList = new List<CorporateClientInfo>();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    CorporateClientInfo objCorporateClientInfo = new CorporateClientInfo();
                    var objCorporateClientList = LImOEntitiesdb.GetCorporateClientList(UserID).ToList();
                    foreach (var CorporateClientDetails in objCorporateClientList)
                    {
                        objCorporateClientInfo = new CorporateClientInfo();
                        objCorporateClientInfo.ID = CorporateClientDetails.ID;
                        objCorporateClientInfo.FirstName = CorporateClientDetails.FirstName;
                        objCorporateClientInfo.Phone = CorporateClientDetails.Phone;
                        objCorporateClientInfo.Email = CorporateClientDetails.Email;
                        objCorporateClientInfo.ProfilePathImg = CorporateClientDetails.ProfilePathImg;
                        CorporateClientList.Add(objCorporateClientInfo);
                    }
                }
                return CorporateClientList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CorporateClientInfo getCorporateClientListByID(int ID, string UserID, string EmailID)
        {
            try
            {
                CorporateClientInfo objCorporateClientInfo = new CorporateClientInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    var CorporateClientDetails = LImOEntitiesdb.GetCorporateClientDetailByID(ID, UserID, EmailID).ToList();
                    foreach (var CorporateClientDetail in CorporateClientDetails)
                    {
                        objCorporateClientInfo.ID = CorporateClientDetail.ID;
                        objCorporateClientInfo.FirstName = CorporateClientDetail.FirstName;
                        objCorporateClientInfo.LastName = CorporateClientDetail.LastName;
                        objCorporateClientInfo.Password = CorporateClientDetail.Password;
                        objCorporateClientInfo.Phone = CorporateClientDetail.Phone;
                        objCorporateClientInfo.Address1 = CorporateClientDetail.Address1;
                        objCorporateClientInfo.Address2 = CorporateClientDetail.Address2;
                        objCorporateClientInfo.CountryId = CorporateClientDetail.CountryId;
                        objCorporateClientInfo.StateId = CorporateClientDetail.StateId;
                        objCorporateClientInfo.CityId = CorporateClientDetail.CityId;
                        objCorporateClientInfo.Email = CorporateClientDetail.Email;
                        objCorporateClientInfo.Gender = CorporateClientDetail.Gender;
                        objCorporateClientInfo.ProfilePathImg = CorporateClientDetail.ProfilePathImg;
                        objCorporateClientInfo.PinCode = CorporateClientDetail.PinCode;
                        objCorporateClientInfo.CompanyName = CorporateClientDetail.CompanyName;
                        objCorporateClientInfo.CompanyRegistrationNo = CorporateClientDetail.CompanyRegistrationNo;
                        objCorporateClientInfo.ContactPersonName = CorporateClientDetail.ContactPersonName;
                        objCorporateClientInfo.ContactDetails = CorporateClientDetail.ContactDetails;
                        objCorporateClientInfo.DOB = Convert.ToDateTime(CorporateClientDetail.DOB).ToString("yyyy-MM-dd");

                    }
                }
                return objCorporateClientInfo;
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteCorporateClientDetail(int ID, string UserID)
        {
            try
            {
                string deleteStatus;
                CorporateClientInfo objCorporateClientInfo = new CorporateClientInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    var CorporateClientDetails = LImOEntitiesdb.DeleteCorporateClient(ID, UserID);
                    deleteStatus = CorporateClientDetails.ToString();
                }
                return deleteStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveCorporateClientDetail(CorporateClientInfo CorporateClientDTO, string UserID)
        {
            try
            {
                CorporateClientInfo objCorporateClientInfo = new CorporateClientInfo();

                using (Entities LImOEntitiesdb = new Entities())
                {
                    if (CorporateClientDTO.ID == 0)
                    {
                        CorporateClientDTO.ID = null;
                    }
                    var CorporateClientDetails = LImOEntitiesdb.SaveCorporateClientDetail(CorporateClientDTO.ID, CorporateClientDTO.FirstName, CorporateClientDTO.LastName, CorporateClientDTO.Password, CorporateClientDTO.Email, CorporateClientDTO.Phone, CorporateClientDTO.Gender, null, CorporateClientDTO.ProfilePathImg, CorporateClientDTO.Address1, CorporateClientDTO.Address2, CorporateClientDTO.CountryId, CorporateClientDTO.StateId, CorporateClientDTO.CityId, CorporateClientDTO.PinCode, CorporateClientDTO.CompanyName, CorporateClientDTO.CompanyRegistrationNo, CorporateClientDTO.ContactPersonName, CorporateClientDTO.ContactDetails, UserID);
                    int? state = null;
                    foreach (var m in CorporateClientDetails)
                    {
                        state = m;
                    }
                    return Convert.ToInt32(state);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveBookingDetail(BookingInfo BookingInfoDTO, string UserID)
        {
            try
            {
                CorporateClientInfo objCorporateClientInfo = new CorporateClientInfo();

                using (Entities LImOEntitiesdb = new Entities())
                {
                    if (BookingInfoDTO.BookingID == 0)
                    {
                        BookingInfoDTO.BookingID = null;
                    }
                    var CorporateClientDetails = LImOEntitiesdb.SaveBookingDetail(BookingInfoDTO.BookingID, BookingInfoDTO.Salutation, BookingInfoDTO.PassengerName, BookingInfoDTO.CountryCode, BookingInfoDTO.ContactNumber, BookingInfoDTO.EmailID, BookingInfoDTO.CountryID, BookingInfoDTO.CityID, BookingInfoDTO.Service, BookingInfoDTO.Car, BookingInfoDTO.Airport, Convert.ToDateTime(BookingInfoDTO.Date), BookingInfoDTO.FlightNumber, BookingInfoDTO.DepartureTimeHH, BookingInfoDTO.DepartureTimeMM, BookingInfoDTO.PickupTimeHH, BookingInfoDTO.PickupTimeMM, BookingInfoDTO.NumberOfPassengers, BookingInfoDTO.NumberOfLuggage, BookingInfoDTO.ExtraStop, BookingInfoDTO.DestinationLocation, BookingInfoDTO.DestinationPostalCode, BookingInfoDTO.Remark, BookingInfoDTO.PickupLocation, BookingInfoDTO.NumberOfHours, BookingInfoDTO.NumberOfDays, UserID);
                    int? state = null;
                    foreach (var m in CorporateClientDetails)
                    {
                        state = m;
                    }
                    return Convert.ToInt32(state);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BookingInfo> getBookingList(string UserID)
        {
            try
            {
                List<BookingInfo> BookingList = new List<BookingInfo>();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    BookingInfo objBookingInfo = new BookingInfo();
                    var Booking = LImOEntitiesdb.GetBookingList(UserID).ToList();
                    foreach (var BookingListDetails in Booking)
                    {
                        objBookingInfo = new BookingInfo();
                        objBookingInfo.BookingID = BookingListDetails.BookingID;
                        objBookingInfo.PassengerName = BookingListDetails.PassengerName;
                        objBookingInfo.ContactNumber = BookingListDetails.ContactNumber;
                        objBookingInfo.Car = BookingListDetails.Car;
                        objBookingInfo.DriverFirstName = BookingListDetails.FirstName;
                        objBookingInfo.DriverLastName = BookingListDetails.LastName;
                        objBookingInfo.Airport = BookingListDetails.Airport;
                        objBookingInfo.DestinationLocation = BookingListDetails.DestinationLocation;
                        objBookingInfo.BookingDate = BookingListDetails.Date;
                        BookingList.Add(objBookingInfo);
                    }
                }
                return BookingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BookingInfo> getBookingListForApi(string Email)
        {
            try
            {
                List<BookingInfo> BookingList = new List<BookingInfo>();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    BookingInfo objBookingInfo = new BookingInfo();
                    var Booking = LImOEntitiesdb.GetBookingListForApi(Email).ToList();
                    foreach (var BookingListDetails in Booking)
                    {
                        objBookingInfo = new BookingInfo();
                        objBookingInfo.BookingID = BookingListDetails.BookingID;
                        objBookingInfo.PassengerName = BookingListDetails.PassengerName;
                        objBookingInfo.ContactNumber = BookingListDetails.ContactNumber;
                        objBookingInfo.Car = BookingListDetails.Car;
                        objBookingInfo.DriverFirstName = BookingListDetails.FirstName;
                        objBookingInfo.DriverLastName = BookingListDetails.LastName;
                        objBookingInfo.Airport = BookingListDetails.Airport;
                        objBookingInfo.DestinationLocation = BookingListDetails.DestinationLocation;
                        objBookingInfo.BookingDate = BookingListDetails.Date;
                        BookingList.Add(objBookingInfo);
                    }
                }
                return BookingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BookingInfo getBookingListByID(int ID, string UserID)
        {
            try
            {
                BookingInfo BookingInfoDTO = new BookingInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    BookingInfoDTO = LImOEntitiesdb.GetBookingDetailsByID(UserID, ID).Select(s => new BookingInfo
                    {
                        BookingID = s.BookingID,
                        Salutation = s.Salutation,
                        PassengerName = s.PassengerName,
                        CountryCode = s.CountryCode,
                        ContactNumber = s.ContactNumber,
                        EmailID = s.EmailID,
                        CountryID = s.CountryID,
                        CityID = s.CityID,
                        Service = s.Service,
                        Car = s.Car,
                        Airport = s.Airport,
                        BookingDate = s.Date,
                        FlightNumber = s.FlightNumber,
                        DepartureTimeHH = s.DepartureTimeHH,
                        DepartureTimeMM = s.DepartureTimeMM,
                        PickupTimeHH = s.PickupTimeHH,
                        PickupTimeMM = s.PickupTimeMM,
                        NumberOfPassengers = s.NumberOfPassengers,
                        NumberOfLuggage = s.NumberOfLuggage,
                        ExtraStop = s.ExtraStop,
                        DestinationLocation = s.DestinationLocation,
                        DestinationPostalCode = s.DestinationPostalCode,
                        PickupLocation = s.PickupLocation,
                        NumberOfHours = s.NumberOfHours,
                        NumberOfDays = s.NumberOfDays,
                        Remark = s.Remark,
                        DriverFirstName = s.firstname,
                        DriverLastName = s.lastname,
                        JobStartedOn = s.JobStartFrom,
                        JobCompletedOn=s.JobCompletedOn,
                        JobStatus=s.jobStatus
                    }).FirstOrDefault();

                    //var BookingDetails = LImOEntitiesdb.GetBookingDetailsByID(UserID,ID).ToList();
                    //foreach (var CorporateClientDetail in BookingDetails)
                    //{
                    //    BookingInfoDTO.BookingID = CorporateClientDetail.BookingID;
                    //    BookingInfoDTO.Salutation = CorporateClientDetail.Salutation;
                    //    BookingInfoDTO.PassengerName = CorporateClientDetail.PassengerName;
                    //    BookingInfoDTO.CountryCode = CorporateClientDetail.CountryCode;
                    //    BookingInfoDTO.ContactNumber = CorporateClientDetail.ContactNumber;
                    //    BookingInfoDTO.EmailID = CorporateClientDetail.EmailID;
                    //    BookingInfoDTO.CountryID = CorporateClientDetail.CountryID;
                    //    BookingInfoDTO.CityID = CorporateClientDetail.CityID;
                    //    BookingInfoDTO.Service = CorporateClientDetail.Service;
                    //    BookingInfoDTO.Car = CorporateClientDetail.Car;
                    //    BookingInfoDTO.Airport = CorporateClientDetail.Airport;
                    //    BookingInfoDTO.Date = Convert.ToDateTime(CorporateClientDetail.Date).ToString("yyyy-MM-dd");
                    //    BookingInfoDTO.FlightNumber = CorporateClientDetail.FlightNumber;
                    //    BookingInfoDTO.DepartureTimeHH = CorporateClientDetail.DepartureTimeHH;
                    //    BookingInfoDTO.DepartureTimeMM = CorporateClientDetail.DepartureTimeMM;
                    //    BookingInfoDTO.PickupTimeHH = CorporateClientDetail.PickupTimeHH;
                    //    BookingInfoDTO.PickupTimeMM = CorporateClientDetail.PickupTimeMM;
                    //    BookingInfoDTO.NumberOfPassengers = CorporateClientDetail.NumberOfPassengers;
                    //    BookingInfoDTO.NumberOfLuggage = CorporateClientDetail.NumberOfLuggage;
                    //    BookingInfoDTO.ExtraStop = CorporateClientDetail.ExtraStop;
                    //    BookingInfoDTO.DestinationLocation = CorporateClientDetail.DestinationLocation;
                    //    BookingInfoDTO.DestinationPostalCode = CorporateClientDetail.DestinationPostalCode;
                    //    BookingInfoDTO.PickupLocation = CorporateClientDetail.PickupLocation;
                    //    BookingInfoDTO.NumberOfHours = CorporateClientDetail.NumberOfHours;
                    //    BookingInfoDTO.NumberOfDays = CorporateClientDetail.NumberOfDays;
                    //    BookingInfoDTO.Remark = CorporateClientDetail.Remark;

                    //}
                }
                return BookingInfoDTO;
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteBookingDetail(int ID, string UserID)
        {
            try
            {
                string deleteStatus;
                CorporateClientInfo objCorporateClientInfo = new CorporateClientInfo();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    var DeleteDetails = LImOEntitiesdb.DeleteBookingDetails(ID, UserID);
                    deleteStatus = DeleteDetails.ToString();
                }
                return deleteStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<JobListModel> getJobList(string UserID)
        {
            try
            {
                List<JobListModel> jobList = new List<JobListModel>();
                using (Entities LImOEntitiesdb = new Entities())
                {
                    JobListModel objJobListInfo = new JobListModel();
                    var Booking = LImOEntitiesdb.GetJobList(UserID).ToList();
                    foreach (var BookingListDetails in Booking)
                    {
                        objJobListInfo = new JobListModel();
                        objJobListInfo.BookingId = BookingListDetails.BookingId.ToString();
                        objJobListInfo.ClientName = BookingListDetails.PassengerName;
                        objJobListInfo.DriverFirstName = BookingListDetails.firstname;
                        objJobListInfo.DriverLastName = BookingListDetails.lastname;
                        objJobListInfo.DateAndTime = BookingListDetails.date;
                        objJobListInfo.JobStatus = BookingListDetails.jobStatus;
                        objJobListInfo.Car = BookingListDetails.car;
                        jobList.Add(objJobListInfo);
                    }
                }
                return jobList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int IsEmailExist(int? ID, string EmailID)
        {
            try
            {
                int Status;
                using (Entities LImOEntitiesdb = new Entities())
                {
                    if (ID == null)
                    {
                        var temp = LImOEntitiesdb.CorporateClients.ToList().Where(x => x.Email == EmailID);
                        Status = temp.Count();
                    }
                    else
                    {
                        Status = LImOEntitiesdb.CorporateClients.ToList().Where(x => x.Email == EmailID && x.ID == ID).Count();
                    }
                }
                return Status;
            }
            catch
            {
                throw;
            }
        }

    }
}
