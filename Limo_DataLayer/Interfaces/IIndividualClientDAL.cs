﻿using Limo_DTO.IndividualClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DataLayer.Interfaces
{
    public interface IIndividualClientDAL
    {
        List<IndividualClientInfo> getIndividualClientList(string UserID);
        IndividualClientInfo getIndividualClientListByID(int ID, string UserID, string EmailID);
        string DeleteIndividualClientDetail(int ID, string UserID);
        int SaveIndividualClientDetail(IndividualClientInfo IndividualClientDTO, string UserID);
        int IsEmailExist(int? ID, string EmailID);
    }
}
