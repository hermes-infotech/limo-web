﻿using Limo_DTO.CorporateClient;
using Limo_DTO.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DataLayer.Interfaces
{
    public interface IDriverDAL
    {
        List<DriverInfo> getDriverList(string UserID);
        DriverInfo getDriverListByID(int ID, string UserID, string EmailID);
        string DeleteDriverDetail(int ID, string UserID);
        int SaveDriverDetail(DriverInfo DriverDTO, string UserID);
        int IsEmailExist(int? ID, string EmailID);
        string DriverAction(DriverJobActionModel djModel, string userId);
        List<JobListModel> GetDriverJobList(int driverid);
        int SaveDriverRating(DriverRatingModel drModel);
    }
}
