﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DTO.Home
{
    public class DriverInfo
    {
        public int ID { get; set; }
        public Nullable<int> TypeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public byte[] ProfilePic { get; set; }
        public string ProfilePathImg { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public Nullable<int> CountryId { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<int> CityId { get; set; }
        public string PinCode { get; set; }
        public string DriverLicense { get; set; }
        public Nullable<byte> Status { get; set; }
    }
}
