﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DTO.CorporateClient
{
    public class JobListModel
    {
        public string BookingId { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverLastName { get; set; }
        public string AssignTo => string.Concat(DriverFirstName, " ", DriverLastName);
        public string ClientName { get; set; }
        public string DateAndTime { get; set; }
        public string JobStatus { get; set; }
        public string Car { get; set; }
        public string PickUpTime { get; set; }
        public string PickUpLocation { get; set; }
        public string DestinationLocation { get; set; }
    }
}
