﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DTO.CorporateClient
{
    public class BookingInfo
    {
        public int? BookingID { get; set; }
        public string Salutation { get; set; }
        public string PassengerName { get; set; }
        public string CountryCode { get; set; }
        public string ContactNumber { get; set; }
        public string EmailID { get; set; }
        public string CountryID { get; set; }
        public string CityID { get; set; }
        public string Service { get; set; }
        public string Car { get; set; }
        public string Airport { get; set; }
        public DateTime? BookingDate { get; set; }
        public string Date => Convert.ToDateTime(BookingDate).ToString("yyyy-MM-dd");
        public string FlightNumber { get; set; }
        public string DepartureTimeHH { get; set; }
        public string DepartureTimeMM { get; set; }
        public string PickupTimeHH { get; set; }
        public string PickupTimeMM { get; set; }
        public string NumberOfPassengers { get; set; }
        public string NumberOfLuggage { get; set; }
        public string ExtraStop { get; set; }
        public string DestinationLocation { get; set; }
        public string DestinationPostalCode { get; set; }
        public string Remark { get; set; }
        public string PickupLocation { get; set; }
        public string NumberOfHours { get; set; }
        public string NumberOfDays { get; set; }
        public Nullable<byte> Status { get; set; }
        public string Flag { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverLastName { get; set; }
        public string DriverFullName => string.Concat(DriverFirstName, " ", DriverLastName);
        public DateTime? JobStartedOn { get; set; }
        public DateTime? JobCompletedOn { get; set; }
        public string JobStatus { get; set; }
    }
}
