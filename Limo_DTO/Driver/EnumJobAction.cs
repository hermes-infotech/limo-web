﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DTO.Driver
{
    public enum EnumJobAction
    {
        Pending=1,
        Accepted =2,
        Inprogress =3,
        Completed =4,
        Declined =5
    }
}
