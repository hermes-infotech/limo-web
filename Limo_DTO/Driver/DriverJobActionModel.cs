﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DTO.Driver
{
    public class DriverJobActionModel
    {
        public int DriverId { get; set; }
        public int JobId { get; set; }
        public string UserId { get; set; }
        public EnumJobAction Action { get; set; }
    }
}
