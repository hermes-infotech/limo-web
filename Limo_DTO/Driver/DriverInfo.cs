﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DTO.Driver
{
    public class DriverInfo
    {
        public int? ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DOB=> DateOfBirth!=null?Convert.ToDateTime(DateOfBirth).ToString("yyyy-MM-dd"):string.Empty;
        public string Gender { get; set; }
        public Nullable<double> WalletAmounts { get; set; }
        public string ProfilePathImg { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public Nullable<int> CountryId { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<int> CityId { get; set; }
        public string PinCode { get; set; }
        public string Status { get; set; }
        public string DriverLicense { get; set; }
        public string DriverLicenseFront { get; set; }
        public string DriverLicenseBack { get; set; }
        public string Flag { get; set; }
        public double Rating { get; set; }
        public int TotolCompletedRides { get; set; }
    }
}
