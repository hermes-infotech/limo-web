﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Limo_DTO.Driver
{
    public class DriverRatingModel
    {
        public int ID { get; set; }
        public int DriverId { get; set; }
        public int JobId { get; set; }
        public String CustomerSignature { get; set; }
        public int Rating { get; set; }        
    }
}
